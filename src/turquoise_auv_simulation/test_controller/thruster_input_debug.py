#!/usr/bin/env python

import rospy
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped


rospy.init_node("thruster_input_listener_node", anonymous=False)


class thruster_listener():
    def __init__(self, i):
        sub = rospy.Subscriber("/turquoise/thrusters/" + str(i)+ "/input", FloatStamped, self.cb)
        self.index = i
        self.data = FloatStamped()
    def cb(self, data):
        #print self.index, "=>", data.data
        self.data = data
        pass




tlist = []
for i in range(8):
    t = thruster_listener(i)
    tlist.append(t)





rate = rospy.Rate(20)
while not rospy.is_shutdown():
    print "------***-------"
    for thruster in tlist:
        print thruster.index, "=>", thruster.data.data
    print "------***-------\n\n"

    rate.sleep()
