import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler

rospy.init_node("node_name", anonymous=False)

pub = rospy.Publisher("/turquoise/waypoint", Path, queue_size=2)


rate = rospy.Rate(10)
while not rospy.is_shutdown():
    print "sending"
    msg1 = PoseStamped()
    msg1.header.frame_id = "world"
    msg1.pose.position.x = 15.65
    msg1.pose.position.y = 9.5
    msg1.pose.position.z = -1.0

    (qx, qy, qz, qw) = quaternion_from_euler(30, 0, 0)
    msg1.pose.orientation.x = qx
    msg1.pose.orientation.y = qy
    msg1.pose.orientation.z = qz
    msg1.pose.orientation.w = qw



    msg2 = PoseStamped()
    msg2.header.frame_id = "world"
    msg2.pose.position.x = 15.75
    msg2.pose.position.y = 10.75
    msg2.pose.position.z = -1.0
    (qx, qy, qz, qw) = quaternion_from_euler(0, 20, 90)
    msg2.pose.orientation.x = qx
    msg2.pose.orientation.y = qy
    msg2.pose.orientation.z = qz
    msg2.pose.orientation.w = qw


    msg = Path()
    msg.header.frame_id = "world"
    msg.poses = [msg1, msg2]

    pub.publish(msg)
    rate.sleep()
