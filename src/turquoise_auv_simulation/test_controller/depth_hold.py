#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import FluidPressure

rospy.init_node("depth_hold", anonymous=False)

msg_out = Twist()
pressure_msg = FluidPressure()

def callback(msg):
    global pressure_msg
    pressure_msg = msg

sub = rospy.Subscriber("/turquoise/pressure", FluidPressure, callback)
pub = rospy.Publisher("/turquoise/cmd_vel", Twist, queue_size=10)

kp = 1.864;
target = 100.0;
vel_limit = -0.05;
tolerance = 0.25;

rate = rospy.Rate(20)
while not rospy.is_shutdown():
    error = target - pressure_msg.fluid_pressure
    if -tolerance < error < tolerance:
        error = 0
    vel = -(kp * error)
    if vel < vel_limit:
        vel = vel_limit

    msg_out.linear.z = vel
    print msg_out
    pub.publish(msg_out)
    rate.sleep()
