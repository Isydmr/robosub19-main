#!/usr/bin/env python
# The script to publish transform from initial_pos to odom frame.
# The script begins to publis, just after ekf starts.


import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped
import tf

initial_pose = None

def callback(msg):
    global initial_pose
    initial_pose = msg


if __name__ == '__main__':
    rospy.init_node('turquoise_tf_broadcaster')

    ref_topic = rospy.get_param('~initial_reference_pos_topic')
    rate_hz = rospy.get_param('~rate')
    timeout = rospy.get_param('~timeout')
    rospy.Subscriber(ref_topic, PoseWithCovarianceStamped, callback)

    listener = tf.TransformListener()
    rospy.loginfo("Waiting for transform")
    listener.waitForTransform("odom", "turquoise/base_link", rospy.Time(), rospy.Duration(timeout))
    rospy.loginfo("Setting Initial Pose %s" % initial_pose)
    pose = initial_pose

    rate = rospy.Rate(rate_hz)
    while not rospy.is_shutdown():
        if initial_pose == None:
            continue
        br = tf.TransformBroadcaster()
        br.sendTransform((pose.pose.pose.position.x, pose.pose.pose.position.y, pose.pose.pose.position.z),
            (pose.pose.pose.orientation.x, pose.pose.pose.orientation.y, pose.pose.pose.orientation.z, pose.pose.pose.orientation.w),
            rospy.Time.now(),
            "odom",
            "world"
            )

        rate.sleep()
