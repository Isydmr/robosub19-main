#! /usr/bin/env python

import rospy
import actionlib
import turquoise_trajectory_controller.msg as action
from TrajectoryController import VBTrajectoryController


class TrajectoryActionServer(object):
    # Feedback and result messages
    feedback = action.SendTrajectoryFeedback()
    result = action.SendTrajectoryResult()

    def __init__(self, name):
        self._action_name = name
        self.action_server = actionlib.SimpleActionServer(self._action_name, action.SendTrajectoryAction, execute_cb=self.execute_cb, auto_start = False)
        self.action_server.start()


    def execute_cb(self, goal):
        begin_time = rospy.Time.now()

        success = True
        # goal = action.SendTrajectoryGoal()
        path = goal.path
        self.controller = VBTrajectoryController()

        self.controller.CancelExecution()
        self.controller.SetTarget(path.poses)
        self.controller.Execute()
        timeout = -1 # set to -1 to disable!


        while not rospy.is_shutdown() and not self.controller.Finished:
            time_elapsed = rospy.Time.now().to_sec() - begin_time.to_sec()
            is_timeout = time_elapsed >= timeout

            # disable timout if -1
            if timeout == -1:
                is_timeout = False

            if self.action_server.is_preempt_requested() or is_timeout:
                # Preemt !
                self.controller.CancelExecution()
                success = False
                rospy.logerr('%s: Preemt/Timeout = (%s/%s)' % (self._action_name, self.action_server.is_preempt_requested(), is_timeout ))
                self.action_server.set_preempted()
                break
            else:
                self.controller.spin_once()
                self.feedback.current_point_index = self.controller.current_position_index
                self.feedback.time_elapsed = rospy.Time.now() - begin_time
                self.feedback.total_points = self.controller.total_points_index
                self.feedback.percentage = self.controller.current_position_index * 100.0 / self.controller.total_points_index
                self.action_server.publish_feedback(self.feedback)


                rospy.loginfo("""%s: Executing trajectory, current index %i/%i - %f/100, Time elapsed: %f"""
                                    % (self._action_name, self.controller.current_position_index,
                                                    self.controller.total_points_index,
                                                    self.controller.current_position_index * 100.0 / self.controller.total_points_index,
                                                    time_elapsed))
        self.controller.CancelExecution()
        if success:
            self.result.time_elapsed = rospy.Time.now() - begin_time
            self.result.total_points = self.controller.total_points_index

            rospy.loginfo("%s: Action Server finished execution in %d seconds." % (self._action_name, rospy.Time.now().secs - begin_time.secs))
            self.action_server.set_succeeded(self.result)


if __name__ == '__main__':
    rospy.init_node("trajectory_controller_action_server")
    server = TrajectoryActionServer("trajectory_action_server")
    rospy.spin()
