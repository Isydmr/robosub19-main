#!/usr/bin/env python

# ROS module
import rospy
# Mavlink ROS messages
from mavros_msgs.msg import Mavlink
# pack and unpack functions to deal with the bytearray
from struct import pack, unpack

rospy.init_node('depthListener', anonymous=True)

# Topic callback
def callback(data):
    # Check if message id is valid (I'm using SCALED_PRESSURE
    # and not SCALED_PRESSURE2)
#    print ":D"
    if data.msgid == 153:
        # rospy.loginfo(rospy.get_caller_id() + " Package: %s", data)
        # Transform the payload in a python string
        p = pack("QQ", *data.payload64)
        # Transform the string in valid values
        # https://docs.python.org/2/library/struct.html
        time_boot_ms, press_abs, press_diff, temperature = unpack("Iffhxx", p)
        # rospy.loginfo(rospy.get_caller_id() + " Package: %s", press_abs)
        print unpack("Iffhxx", p)
        # TODO: Add depth message
rospy.Subscriber("/mavlink/from", Mavlink, callback)
    
rospy.spin()
