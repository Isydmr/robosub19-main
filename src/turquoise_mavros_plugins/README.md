# Turquoise Underwater MAVROS Parameter settings and plugins
This package consists of necessary launch files and scripts to connect the main computer to the pixhawk using mavros.



## Main Usage
```
roslaunch turquoise_mavros_plugins apm.launch
```

### Keep in Mind !

GPS_TYPE Parameter needs to be set to MAV (14) to be able to use the fake_gps


rosrun mavros mavsys rate --all 50



## Basic Usage (DEPRECEATED!)
Begin connection:
```
roslaunch mavros apm.launch fcu_url:=udp://:14550@192.168.2.2:14551
```


Arm the vehicle and change mode to manual:
```
rosrun mavros mavsafety arm
rosrun mavros mavsys mode -c manual
```
