import json


file = open("t100.csv", "r+")
raw_lines = file.read().split("\n")
file.close()
t100 = {

}

gravity = 9.80665
for line in raw_lines:
    values = []
    if "," in line and "#" not in line:
        values = line.split(",")
    else:
        continue
    t100[str(values[0])] = {
        "lbf": float(values[1]),
        "rpm": float(values[2]),
        "kgf": float(values[3]),
        "newtons": float(values[3]) * gravity
    }

# print t100
_js = json.dumps(t100, indent=4, sort_keys=True)
file = open("t100.json", "w+")
file.write(_js)
file.close()
