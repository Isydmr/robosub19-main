#!/usr/bin/env python
import rospy
from sensor_msgs.msg import FluidPressure
from sensor_msgs.msg import Temperature
from std_msgs.msg import Float32
from board.msg import BoardIn
import yaml

class Bar30Node(object):
    """docstring for Bar30Node."""

    def __init__(self):
        self.depth_pub = rospy.Publisher("/turquoise/sensors/bar30/depth", Float32, queue_size=1)
        self.pres_pub = rospy.Publisher("/turquoise/sensors/bar30/pressure", FluidPressure, queue_size=1)
        self.temp_pub = rospy.Publisher("/turquoise/sensors/bar30/temperature", Temperature, queue_size=1)
        self.sub = rospy.Subscriber("/turquoise/board/in", BoardIn, self.callback)

    def callback(self, data):
        msg = ""
        try:
            msg = yaml.safe_load(data.message)
        except yaml.YAMLError, exc:
            if hasattr(exc, 'problem_mark'):
                mark = exc.problem_mark
                print "MSG PARSING ERROR, Error position: (%s:%s)" % (mark.line+1, mark.column+1)
                print data
                return

        if "bar30" in msg.keys():
            temp = msg["bar30"]["temperature"]
            pres = msg["bar30"]["pressure"]
            depth = msg["bar30"]["depth"]

            msg = Float32()
            msg.data = depth
            self.depth_pub.publish(msg)

            msg = Temperature()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "bar30_link"
            msg.temperature = temp
            self.temp_pub.publish(msg)

            msg = FluidPressure()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "bar30_link"
            msg.fluid_pressure = pres
            self.pres_pub.publish(msg)


if __name__ == "__main__":
    rospy.init_node("bar30_node")
    node = Bar30Node()
    rospy.spin()
