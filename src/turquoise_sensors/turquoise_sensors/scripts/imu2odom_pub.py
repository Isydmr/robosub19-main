#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from board.msg import BoardIn
import yaml
global depth
rospy.init_node("imu2odom")
pub = rospy.Publisher("/turquoise/odom", Odometry, queue_size=1)

def d_cb(data):
    global depth
    jstr = "{ " + data.message + " }"
    depth = yaml.safe_load(jstr)["bar30"]["depth"]


dep_sub = rospy.Subscriber("/turquoise/board/in", BoardIn, d_cb)
def cb(data):
    global depth
    msg = Odometry()
    msg.header = data.header
#    msg.pose.pose.position.z = -depth
    msg.pose.pose.orientation = data.orientation
    msg.twist.twist.angular.x = data.angular_velocity.x
    msg.twist.twist.angular.y = data.angular_velocity.y
    msg.twist.twist.angular.z = data.angular_velocity.z
    global pub
    pub.publish(msg)

sub = rospy.Subscriber("/turquoise/sensors/imu/data", Imu, cb)
rospy.spin()
