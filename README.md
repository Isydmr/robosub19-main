[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![ROS: Kinetic](https://img.shields.io/badge/ROS-Kinetic-red.svg)](https://wiki.ros.org/kinetic) [![web: ituauv.com](https://img.shields.io/badge/web-ituauv.com-blue.svg)](https://www.ituauv.com) [![pipeline status](https://gitlab.com/itu-auv/robosub19-main/badges/master/pipeline.svg)](https://gitlab.com/itu-auv/robosub19-main/commits/master)

Turquoise AUV
===============
This repository consists of the software developed by [ITU AUV Team](https://www.ituauv.com), for the international autonomous underwater vehicle competition Robosub'19. The team consits of 20-25 undergraduate students from multiple departments. Our aim is to perform our best to compete in
Robosub and have good experience, also while preparing, we'd like to share our knowledge with the others. For any further questions feel free to
contact us by <contact@ituauv.com> or contact me by <senceryazici@gmail.com>


Installation Instructions - Ubuntu 16.04 with ROS Kinetic
---------------------------------------------------------
 ```
 $ cd ~
 $ git clone https://gitlab.com/itu-auv/robosub19-main.git auv_ws
 $ cd ~/auv_ws
 $ git submodule update --init --recursive
 $ catkin build
 $ echo "source ~/auv_ws/devel/setup.bash" >> ~/.bashrc
 $ source ~/.bashrc
 ```

Dependencies
---
Project is currently being developed and the dependencies are constantly being updated, see the [ci file](.gitlab-ci.yaml) for the latest dependencies.

Basic Usage
-----------
#### Teleop Control:
  ##### Simulation:
  ```
  $ roslaunch turquoise_description robosub.launch
  $ roslaunch turquoise_description upload.launch
  $ roslaunch turquoise_control teleop_control_sim.launch
  ```
  ##### On Vehicle:
  ```
  $ roslaunch turquoise_mavros_plugins apm.launch
  $ roslaunch turquoise_control teleop_control.launch
  ```


#### Localization Package:
The ORB-SLAM can be launched in the localization launch file, may needs to be commented out for both launch and ekf parameter files.
  ```
  $ roslaunch turquoise_localization localization.launch
  ```


#### Trajectory Controller Package:
Trajectory controller package is still in developement however, except the minor fixes, the package overall works fine.
The [test.launch](src/turquoise_auv_simulation/turquoise_trajectory_controller/launch/test.launch) file consists of a action server and an example of action client.
  ```
  $ roslaunch turquoise_trajectory_controller test.launch
  ```

Docker Build and Run
-----------
  ```
  # Move to the directory containing the Dockerfile and execute the following
  $ sudo docker build -t "auv:latest" . # builds the docker image
  $ sudo docker run -it auv bash # runs the docker image's command line
  ```


Description
-----------
[uuv_simulator](src/uuv_simulator) package is developed by [uuvsimulator](https://github.com/uuvsimulator) team, a Gazebo/ROS packages for underwater robotics simulation [uuvsimulator.github.io](https://uuvsimulator.github.io/)

[zed-ros-wrapper](src/zed-ros-wrapper) repository is developed by [Stereolabs](https://github.com/stereolabs/zed-ros-wrapper) Company for ZED stereo camera.

Authors
---
This repository is being developed and maintained by the members of [ITU AUV Team](https://www.ituauv.com)

License
-------
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
