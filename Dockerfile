FROM ros:kinetic-ros-base-xenial

# install ros packages
RUN apt-get update && apt-get install -y \
    ros-kinetic-robot=1.3.2-0* \
    python-catkin-tools \
    ros-kinetic-tf \
    ros-kinetic-cv-bridge \
    ros-kinetic-image-geometry \
    ros-kinetic-image-transport \
    ros-kinetic-camera-info-manager \
    ros-kinetic-tf2-geometry-msgs \
    libgtk2.0-dev \
    ros-kinetic-gazebo-* \
    protobuf-compiler protobuf-c-compiler
# clone and build the repo
RUN git clone https://gitlab.com/itu-auv/robosub19-main.git
RUN cd robosub19-main && git submodule update --init --recursive
RUN cd robosub19-main && catkin config --extend /opt/ros/kinetic && catkin build --summarize --no-status --force-color
RUN rm -rf /var/lib/apt/lists/*
